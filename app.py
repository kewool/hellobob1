from flask import Flask, request

app = Flask(__name__, template_folder="pages/")

@app.route("/", methods=["GET"])
def main():
    return {"message":"hello bob from user004"}

@app.route("/add", methods=["GET"])
def add():
    return str(int(request.args.get("a")) + int(request.args.get("b")))

@app.route("/sub", methods=["GET"])
def sub():
    return str(int(request.args.get("a")) - int(request.args.get("b")))

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8004)
