from unittest import TestCase, main
import app

class test(TestCase):
    def setUp(self):
        self.app = app.app.test_client()
        self.params={"a":3,"b":1}
    def test_add(self):
        res = self.app.get("/add", query_string=self.params)
        self.assertEqual(int(res.text), self.params["a"] + self.params["b"])
    def test_sub(self):
        res = self.app.get("/sub", query_string=self.params)
        self.assertEqual(int(res.text), self.params["a"] - self.params["b"])

if __name__ == "__main__":
    main()
